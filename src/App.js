import React from 'react'
import {Provider} from 'react-redux'
import {createStore} from 'redux'
import reducers from './reducers'
import MainScreen from './components/main_screen'

const App = () => {
    return (
        <Provider store={createStore(reducers)}>
            <MainScreen/>
        </Provider>
    )
};

export default App