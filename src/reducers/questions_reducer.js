import questions from './questions.json'
import {
    ALL_QUESTION,
    REMAINING_QUESTIONS,
    ANSWERED_QUESTIONS,
    CURRENT_QUESTION,
    START_SESSION,
    STOP_SESSION,
    NEXT_QUESTION
} from '../actions';

const INITIAL_STATE = {
    [ALL_QUESTION]: questions,
    [REMAINING_QUESTIONS]: questions,
    [ANSWERED_QUESTIONS]: [],
    [CURRENT_QUESTION]: null
};

export default (state = INITIAL_STATE, action) => {
    let newQuestionsObj = {};
    switch (action.type) {
        case START_SESSION:
            newQuestionsObj = generateNewQuestion(state[ALL_QUESTION]);
            return {
                ...state,
                [REMAINING_QUESTIONS]: newQuestionsObj.rest,
                [ANSWERED_QUESTIONS]: [],
                [CURRENT_QUESTION]: newQuestionsObj.newQuestion
            };
        case STOP_SESSION:
            return {
                ...state,
                [ANSWERED_QUESTIONS]: state[CURRENT_QUESTION] ? [state[CURRENT_QUESTION], ...state[ANSWERED_QUESTIONS]] : state[ANSWERED_QUESTIONS],
                [CURRENT_QUESTION]: null
            };
        case NEXT_QUESTION:
            newQuestionsObj = generateNewQuestion(state[REMAINING_QUESTIONS]);
            return {
                ...state,
                [REMAINING_QUESTIONS]: newQuestionsObj.rest,
                [ANSWERED_QUESTIONS]: [state[CURRENT_QUESTION], ...state[ANSWERED_QUESTIONS]],
                [CURRENT_QUESTION]: newQuestionsObj.newQuestion
            };
        default:
            return state;
    }
}

function generateNewQuestion(remainingQuestions) {
    let [newQuestion, ...rest] = shuffle(remainingQuestions);
    return {
        newQuestion,
        rest
    }
}

function shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}