import {combineReducers} from 'redux'
import SettingReducer from './settings_reducer'
import QuestionsReducer from './questions_reducer'
import SessionReducer from './session_reducer'

export default combineReducers({
    settings: SettingReducer,
    questions: QuestionsReducer,
    session: SessionReducer
})