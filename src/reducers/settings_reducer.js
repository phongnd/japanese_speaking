import {UPDATE_SETTING, SESSION_TIME, QUESTION_TIME, ENABLE_RECORDING} from '../actions/types';

const INITIAL_STATE = {
    [SESSION_TIME]: 300,
    [QUESTION_TIME]: 20,
    [ENABLE_RECORDING]: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case UPDATE_SETTING:
            value = parseInt(action.payload.settingValue);
            return {
                ...state,
                [action.payload.settingName]: isNaN(value) ? 0 : value
            };
        default:
            return state
    }
}