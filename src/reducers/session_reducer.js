import {
    START_SESSION,
    STOP_SESSION,
    NEXT_QUESTION,
    COUNTDOWN,
    SESSION_REMAINING_TIME,
    QUESTION_REMAINING_TIME,
    SESSION_TIME,
    QUESTION_TIME,
    IS_RUNNING
} from '../actions/types'

const INITIAL_STATE = {
    [SESSION_REMAINING_TIME]: 0,
    [QUESTION_REMAINING_TIME]: 0,
    [QUESTION_TIME]: 0,
    [IS_RUNNING]: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case START_SESSION:
            console.log(action)
            let {settings} = action.payload;
            return {
                ...state,
                [SESSION_REMAINING_TIME]: settings[SESSION_TIME],
                [QUESTION_REMAINING_TIME]: settings[QUESTION_TIME],
                [QUESTION_TIME]: settings[QUESTION_TIME],
                [IS_RUNNING]: true
            };
        case STOP_SESSION:
            return INITIAL_STATE;
        case NEXT_QUESTION:
            return {
                ...state,
                [QUESTION_REMAINING_TIME]: state[QUESTION_TIME]
            };
            return state;
        case COUNTDOWN:
            if (action.payload.countdownType === SESSION_REMAINING_TIME) {
                return {
                    ...state,
                    [SESSION_REMAINING_TIME]: state[SESSION_REMAINING_TIME] - 1
                }
            }
            if (action.payload.countdownType === QUESTION_REMAINING_TIME) {
                return {
                    ...state,
                    [QUESTION_REMAINING_TIME]: state[QUESTION_REMAINING_TIME] - 1
                }
            }

            return state;
        default:
            return state;
    }
}