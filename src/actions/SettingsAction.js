import {UPDATE_SETTING} from './types'

export function updateSetting(settingName, settingValue) {
    return {
        type: UPDATE_SETTING,
        payload: {
            settingName,
            settingValue,
        }
    }
}