export const START_SESSION = 'start_session';
export const STOP_SESSION = 'stop_session';
export const NEXT_QUESTION = 'next_question';
export const COUNTDOWN = 'countdown';

export const UPDATE_SETTING = 'update_setting';
export const SESSION_TIME = 'session_time';
export const QUESTION_TIME = 'question_time';
export const ENABLE_RECORDING = 'enable_recording';

export const SESSION_REMAINING_TIME = 'session_remaining_time';
export const QUESTION_REMAINING_TIME = 'question_remaining_time';
export const IS_RUNNING = 'is_running';

export const ALL_QUESTION = 'all_questions';
export const REMAINING_QUESTIONS = 'remaining_questions';
export const ANSWERED_QUESTIONS = 'answered_questions';
export const CURRENT_QUESTION = 'current_question';