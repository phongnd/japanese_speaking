import {NEXT_QUESTION, START_SESSION, STOP_SESSION, COUNTDOWN} from './types'

export function startSession(settings = {}) {
    return {
        type: START_SESSION,
        payload: {
            settings
        }
    }
}

export function stopSession() {
    return {
        type: STOP_SESSION
    }
}

export function countDown(countdownType) {
    return {
        type: COUNTDOWN,
        payload: {
            countdownType
        }
    }
}

export function nextQuestion() {
    return {
        type: NEXT_QUESTION
    }
}