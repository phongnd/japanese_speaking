import React, {Component} from 'react'
import {Container, Content, Title, Header, Body, Text} from 'native-base';
import TimeCounter from './common/time_counter'
import Question from './common/question'
import Settings from './common/settings'

export default class MainScreen extends Component {
    render() {
        return (
            <Container>
                <Header>
                    <Body>
                        <Title>Speaking Test</Title>
                    </Body>
                </Header>
                <Content>
                    <TimeCounter/>
                    <Question/>
                    <Settings/>
                </Content>
            </Container>
        )
    }
}