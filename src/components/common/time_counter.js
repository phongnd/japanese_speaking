import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Card, CardItem, Text} from 'native-base'
import {Col, Grid} from 'react-native-easy-grid';
import {SESSION_REMAINING_TIME, QUESTION_REMAINING_TIME, stopSession} from '../../actions'

class TimeCounter extends Component {
    render() {
        return (
            <Card>
                <CardItem>
                    <Grid>
                        <Col>
                            <Text>Session Time: {this.props[SESSION_REMAINING_TIME]}</Text>
                        </Col>
                        <Col>
                            <Text style={{textAlign: 'right'}}>Question Time: {this.props[QUESTION_REMAINING_TIME]}</Text>
                        </Col>
                    </Grid>
                </CardItem>
            </Card>
        )
    }
}

function mapStateToProps(state) {
    return {
        [SESSION_REMAINING_TIME]: state.session[SESSION_REMAINING_TIME],
        [QUESTION_REMAINING_TIME]: state.session[QUESTION_REMAINING_TIME]
    }
}

export default connect(mapStateToProps)(TimeCounter)