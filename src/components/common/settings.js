import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Card, CardItem, Body, Form, Item, Label, Input, Text, ListItem, Switch, Right} from 'native-base'
import {Col, Grid} from 'react-native-easy-grid';
import {updateSetting} from '../../actions/SettingsAction'
import {SESSION_TIME, QUESTION_TIME, ENABLE_RECORDING, IS_RUNNING} from '../../actions/types'

class Settings extends Component {
    renderSettings() {
        return (
            <Card>
                <CardItem header>
                    <Text>Settings</Text>
                </CardItem>
                <CardItem style={{paddingTop: 0, paddingBottom: 0}}>
                    <Grid>
                        <Col>
                            <Label style={{marginTop: 13, marginBottom: 13}}>Time Per Session</Label>
                        </Col>
                        <Col>
                            <Item underline>
                                <Input placeholder="seconds"
                                       onChangeText={(value) => this.props.updateSetting(SESSION_TIME, value)}
                                       value={this.props[SESSION_TIME].toString()}/>
                            </Item>
                        </Col>
                    </Grid>
                </CardItem>
                <CardItem style={{paddingTop: 0, paddingBottom: 0}}>
                    <Grid>
                        <Col>
                            <Label style={{marginTop: 13, marginBottom: 13}}>Time Per Question</Label>
                        </Col>
                        <Col>
                            <Item underline>
                                <Input placeholder="seconds"
                                       onChangeText={(value) => this.props.updateSetting(QUESTION_TIME, value)}
                                       value={this.props[QUESTION_TIME].toString()}/>
                            </Item>
                        </Col>
                    </Grid>
                </CardItem>
                <CardItem style={{paddingTop: 0, paddingBottom: 0, height: 50}}>
                    <Grid>
                        <Col>
                            <Label style={{marginTop: 13, marginBottom: 13}}>Enable Recording</Label>
                        </Col>
                        <Col>
                            <Switch style={{marginTop: 13, marginBottom: 13}} value={this.props[ENABLE_RECORDING]}
                                    onValueChange={(value) => this.props.updateSetting(ENABLE_RECORDING, value)}/>
                        </Col>
                    </Grid>
                </CardItem>
            </Card>
        )
    }

    render() {
        return (this.props.isRunning ? null : this.renderSettings())
    }
}

function mapStateToProps(state) {
    console.log(state)
    return {
        [SESSION_TIME]: state.settings[SESSION_TIME],
        [QUESTION_TIME]: state.settings[QUESTION_TIME],
        [ENABLE_RECORDING]: state.settings[ENABLE_RECORDING],
        isRunning: state.session[IS_RUNNING]
    }
}

export default connect(mapStateToProps, {updateSetting})(Settings)