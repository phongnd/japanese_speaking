import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Card, CardItem, Button, Text} from 'native-base'
import {Col, Row, Grid} from 'react-native-easy-grid';
import {
    SESSION_REMAINING_TIME,
    QUESTION_REMAINING_TIME,
    IS_RUNNING,
    CURRENT_QUESTION,
    ANSWERED_QUESTIONS,
    startSession,
    stopSession,
    nextQuestion,
    countDown
} from '../../actions'

class Question extends Component {
    sessionInterval = null;
    questionInterval = null;

    countDown() {
        this.sessionInterval = setInterval(() => {
            if (this.props.sessionTime === 0) {
                this.props.stopSession();
                clearInterval(this.sessionInterval);
                clearInterval(this.questionInterval);
            } else {
                this.props.countDown(SESSION_REMAINING_TIME)
            }
        }, 1000);
        this.questionInterval = setInterval(() => {
            if (this.props.questionTime === 1) {
                this.props.nextQuestion()
            } else {
                this.props.countDown(QUESTION_REMAINING_TIME)
            }
        }, 1000);
    }

    onClickStart() {
        this.props.startSession(this.props.settings);
        this.countDown();
    }

    renderStartButton() {
        return (
            <Grid>
                <Col style={{paddingLeft: 5, paddingRight: 5}}>
                    <Button primary full rounded onPress={this.onClickStart.bind(this)}><Text> Start </Text></Button>
                </Col>
            </Grid>
        )
    }

    renderNextButton() {
        return (
            <Grid>
                <Col style={{paddingLeft: 5, paddingRight: 5}}>
                    <Button danger full rounded onPress={() => this.props.stopSession()}><Text> Stop </Text></Button>
                </Col>
                <Col style={{paddingLeft: 5, paddingRight: 5}}>
                    <Button primary full rounded onPress={() => this.props.nextQuestion()}><Text> Next </Text></Button>
                </Col>
            </Grid>
        )
    }

    renderAnswers() {
        return (
            this.props.answeredQuestions.length > 0 ?
                (
                    <CardItem>
                        <Grid>
                            {this.props.answeredQuestions.map((q, i) => {
                                return (
                                    <Row key={`question_${i}`}>
                                        <Card>
                                            <CardItem header>
                                                <Text>{q.question}</Text>
                                            </CardItem>
                                            <CardItem>
                                                <Text>{q.answer}</Text>
                                            </CardItem>
                                        </Card>
                                    </Row>
                                )
                            })}
                        </Grid>
                    </CardItem>
                ) :
                null
        )
    }

    render() {
        return (
            <Card>
                <CardItem>
                    <Text style={{
                        flex: 1,
                        textAlign: 'center'
                    }}>{this.props.isRunning ? this.props.question.question : "Press Start to start the test."}</Text>
                </CardItem>
                <CardItem>
                    {(this.props.isRunning) ? this.renderNextButton() : this.renderStartButton()}
                </CardItem>
                {this.renderAnswers()}
            </Card>
        )
    }
}

function mapStateToProps(state) {
    return {
        question: state.questions[CURRENT_QUESTION],
        answeredQuestions: state.questions[ANSWERED_QUESTIONS],
        settings: state.settings,
        isRunning: state.session[IS_RUNNING],
        sessionTime: state.session[SESSION_REMAINING_TIME],
        questionTime: state.session[QUESTION_REMAINING_TIME]
    }
}

export default connect(mapStateToProps, {startSession, stopSession, nextQuestion, countDown})(Question)