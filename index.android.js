import {AppRegistry} from 'react-native';
import App from './src/App';

AppRegistry.registerComponent('japanese_speaking', () => App);
